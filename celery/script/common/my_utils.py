import re
import sys
import os
import logging
from calendar import monthrange
logging.basicConfig(filename='debug.log', level=logging.DEBUG, format='%(asctime)s:%(levelname)s:%(message)s')


def print_error(error):
    exc_tb = sys.exc_info()[2]
    error = f"{os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]}, {exc_tb.tb_lineno}, {error}"
    logging.warning(error)
    raise


def print_debug(message):
    logging.debug(message)


def print_info(message):
    logging.info(message)


def recursive_items(dictionary):
    for key, value in dictionary.items():
        if type(value) is dict:
            yield from recursive_items(value)
        else:
            yield (key, value)


def extract_fully(data, store):
    for key, value in recursive_items(data):
        if type(value) is list:
            store[key] = []
            for va in value:
                if type(va) is dict:
                    extract_fully(va, store)
                else:
                    store[key].append(va)
            if len(store[key]) <= 0:
                del store[key]
            return
        else:
            store[key] = value


def extract_nested_dict(data):
    result = {}
    extract_fully(data, result)
    return result


def month_last_date(year=2010, month=1):
    return monthrange(year, month)[1]


def str_to_num_list(string):
    return re.findall(r'\d+', string)


def as_datetime_look(string_list):
    if len(string_list) < 3:
        return None
    for _ in range(len(string_list), 7):
        string_list.append('0')
    return string_list


def str_date_or_time_to_datetime_list(string):
    string_list = str_to_num_list(string)
    string_list = as_datetime_look(string_list)
    date_list = [int(x) for x in string_list]
    return date_list


def convert_date_time(datetime):
    str_datetime = str(datetime)
    date_list = str_date_or_time_to_datetime_list(str_datetime)
    return datetime.datetime(*date_list)  # send datetime as a list of argument


def remove_datetime_error(data):
    data = list(data)
    for i in range(0, len(data)):
        if type(data[i]).__name__ in 'datetime':
            data[i] = convert_date_time(data[i])
    return data


def make_dict(drrs_key, data):
    data_list = []
    for x in data:
        x = remove_datetime_error(x)
        data_list.append(dict(zip(drrs_key, x)))
    return data_list
