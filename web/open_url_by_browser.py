import webbrowser
import numpy as nu
import pandas as pd

file = "../data/kaggle_datasets.csv"
data = pd.read_csv(file)

urls = data["url"].values
webbrowser.open_new_tab(urls[0])
webbrowser.open_new_tab(urls[10])
webbrowser.open_new_tab(urls[20])
webbrowser.open_new_tab(urls[30])