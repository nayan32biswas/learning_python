import card
import Sum
import inputinteger


class Account:
    balance = 0

    def __init__(self, balance=1000):
        self.balance = balance

    def withdraw(self, balance):
        if self.balance >= balance:
            self.balance -= balance
            return True
        return False

    def deposit(self, balance):
        self.balance += balance


class Game(Account):
    card_list = list()

    player_list = list()
    computer_list = list()

    player_sum = 0
    computer_sum = 0

    turn = True
    deal = 5

    def __init__(self):
        balance = inputinteger.take("Input initial amount: ")[0]
        Account.__init__(self, balance)

    def show_each_player(self, _card_list, _sum):
        if type(_card_list) == str:
            print(f"{_card_list:10}  ---   {_sum}")
        else:
            for index, value in enumerate(_card_list):
                if index == len(_card_list) // 2:
                    print(f"{value:10}  ---   {_sum}")
                else:
                    print(f"{value:10}")
        return

    def show_player(self):
        print("")
        if self.turn:
            self.computer_sum = Sum._sum(self.computer_list[1])
            print("Computer Score: ")
            self.show_each_player(self.computer_list[1], self.computer_sum)
        else:
            self.computer_sum = Sum._sum(self.computer_list)
            print("Computer Score: ")
            self.show_each_player(self.computer_list, self.computer_sum)

        self.player_sum = Sum._sum(self.player_list)
        print("\nPlayer Score: ")
        self.show_each_player(self.player_list, self.player_sum)
        print("\n")
        return

    def split_card(self):
        self.computer_list.append(self.card_list.pop())
        self.computer_list.append(self.card_list.pop())

        self.player_list.append(self.card_list.pop())
        self.player_list.append(self.card_list.pop())
        self.show_player()

    def player_win(self):
        print("\n     Player win")
        self.deposit(2 * self.deal)

    def each_game(self):
        self.card_list = list()

        self.player_list = list()
        self.computer_list = list()

        self.player_sum = 0
        self.computer_sum = 0

        self.deal = 0
        self.turn = True

        self.card_list = card.getcardlist()
        while True:
            self.deal = inputinteger.take("Input DEAL: ")[0]
            if self.withdraw(self.deal):
                break
            else:
                print(f"You have {self.balance} taka only")
        self.split_card()

        print("-------------------player turn-------------------------")

        self.player_round()
        if self.player_sum > 21:
            print("\n      Computer win")
            return
        self.turn = False
        self.computer_round()

        if self.computer_sum == self.player_sum:
            print("\n      The game is tied")
            self.deposit(self.deal)
            return

        if self.computer_sum > 21 or self.player_sum > self.computer_sum:
            self.player_win()
            return
        else:
            print("\n      Computer win")
            return

    def add_card(self):
        if self.turn:
            self.player_list.append(self.card_list.pop())
        else:
            self.computer_list.append(self.card_list.pop())
        self.show_player()

    def player_round(self):
        while True:
            temp = input("Hit or Stand? Enter h or s: ")
            if "h" in set(temp):
                self.add_card()
                if self.player_sum > 21:
                    return
            elif "s" in set(temp):
                break

    def computer_round(self):
        print("-------------------computer turn-------------------------")
        self.show_player()
        while self.computer_sum < 17 and self.computer_sum < self.player_sum:
            # input("Input any character: ")
            self.add_card()

    def start(self):
        n = 1
        demo = ("1'st Game start", "2'nd Game start", "3'rd Game start", "'th Game start")
        while self.balance > 0:
            print(f"  current balance is {self.balance}")
            if n <= 3:
                print(f"          {demo[n-1]}")
            else:
                print(f"          {str(n)+demo[3]}")
            self.each_game()
            print("-------------------game finish-------------------------")
            n += 1


game = Game()
game.start()
