board = [
    ["X", "X", "O"],
    ["O", "O", "X"],
    ["X", "O", "X"]
]
player = " "


def cleanUp(symbol):
    global board
    global player
    for row, value in enumerate(board):
        for clo, x in enumerate(value):
            board[row][clo] = " "
    player = symbol


def showBoard():
    global board
    print("\n-----------")
    print(" " + board[0][0] + " | " + board[0][1] + " | " + board[0][2])
    print("---|---|---")
    print(" " + board[1][0] + " | " + board[1][1] + " | " + board[1][2])
    print("---|---|---")
    print(" " + board[2][0] + " | " + board[2][1] + " | " + board[2][2])
    print("-----------\n")


square = 3


def validPos(x, y, s):
    global board
    return 0 <= x < square and 0 <= y < square and board[x][y] == s


def validLine(x, y):
    global board
    if board[x][y] == " ":
        return False
    if x == 0 and y == 0:
        coun = 0
        i = x
        j = y
        while validPos(i, j, board[x][y]):
            i += 1
            j += 1
            coun += 1
        if coun >= 3:
            return True
    if x == 2 and y == 0:
        coun = 0
        i = x
        j = y
        while validPos(i, j, board[x][y]):
            i -= 1
            j += 1
            coun += 1
        if coun >= square:
            return True

    coun = 0
    i = x
    while validPos(i, y, board[x][y]):
        i += 1
        coun += 1
    if coun >= square:
        return True

    coun = 0
    j = y
    while validPos(x, j, board[x][y]):
        j += 1
        coun += 1
    if coun >= square:
        return True


def isWine():
    for i in range(3):
        if validLine(i, 0) or validLine(i, 1) or validLine(i, 2):
            return True
    return False


def isDrow():
    global board
    for row, value in enumerate(board):
        for clo, x in enumerate(value):
            if board[row][clo] == " ":
                return False
    return True


def markBoard(posX, posY):
    global board
    global player
    if posX<0 or posX>2 or posY<0 or posY>2:
        print("Invalid input")
        return
    if board[posX][posY] != " ":
        print("This cell already marked")
        return
    board[posX][posY] = player
    showBoard()
    if player == "O":
        player = "X"
    else:
        player = "O"


def start():
    s = input("Input player 1 Symbol: ")
    cleanUp(s)
    showBoard()
    while True:
        lll = input("Input cell in x, y: ")
        lll = [int(s) for s in lll.split() if s.isdigit()]
        markBoard(lll[0], lll[1])
        if isWine():
            print(f"Player {board[lll[0]][lll[1]]} win")
            return
        if isDrow():
            print("Match is draw")
            return


while True:
    print("if you want to play input \"start\" other wise \"exit\"")
    if "".join(input().split()) == "exit":
        break
    start()