from bs4 import BeautifulSoup as soup
from urllib.request import urlopen

my_url = "https://www.flipkart.com/search?q=iphone&otracker=search&otracker1=search&marketplace=FLIPKART&as-show=on&as=off"

uClient = urlopen(my_url)
page_html = uClient.read()
uClient.close()
page_soup = soup(page_html, "html.parser")

containers = page_soup.findAll("div", {"class": "_1UoZlX"})

# print(len(containers))
# print(soup.prettify(containers[0]))

# container = containers[0]
# print(container.img["alt"])

# price = container.findAll("div", {"class": "col col-5-12 _2o7WAb"})
# print(price[0].text)
# reating = container.findAll("div", {"class": "niH0FQ"})
# print(reating[0].text)

# price_container = container.findAll("div", {"class": "col col-5-12 _2o7WAb"})
# price = price_container[0].text.strip()
# price = "".join(price.split(",")).split("₹")[1]
# print(price)



filename = "product_list.csv"
f = open(filename, "w")
header = "Product Name, Price, Reating\n"


f.write(header)
for container in containers:
    product_name = container.img["alt"]
    product_name = product_name.replace(",", "")

    price_container = container.findAll("div", {"class": "col col-5-12 _2o7WAb"})
    price = price_container[0].text.strip()
    price = "".join(price.split(",")).split("₹")[1]
    price = price.split("E")[0]


    reating_container = container.findAll("div", {"class": "niH0FQ"})
    reating = reating_container[0].text
    reating = reating.replace(",", " ").split(" ")[0]

    colm = product_name+","+price+","+reating+"\n"
    # print(colm)
    f.write(colm)
    