import pandas as pd
import numpy as np
import cufflinks as cf
import matplotlib.pyplot as plt
import plotly.graph_objs as go
from plotly.offline import download_plotlyjs, plot, iplot

df = pd.read_csv("911.csv")

print(df.head())
import seaborn as sns
# import plotly.graph_objs as go
# from plotly.offline import download_plotlyjs, plot, iplot

df = pd.read_csv("../data/911.csv")


result = df['zip'].value_counts().head(5)

result = df['twp'].value_counts().head(5)

result = len(df['title'].unique())

df["Reason"] = df['title'].apply(lambda title: title.split(":")[0])

result = df['Reason'].value_counts().head(5)

# sns.countplot(x='Reason', data=df)

df['timeStamp'] = pd.to_datetime(df['timeStamp'])

df['Hour'] = df['timeStamp'].apply(lambda time: time.hour)
df['Month'] = df['timeStamp'].apply(lambda time: time.month)
df['Day of Week'] = df['timeStamp'].apply(lambda time: time.dayofweek)
dmap = {0: "Mon", 1: "Tue", 2: "Wed", 3: "Thu", 4: "Fir", 5: "Sat", 6: "Sun"}

df['Day of Week'] = df['Day of Week'].map(dmap)


# sns.countplot(x='Day of Week', data=df, hue="Reason", palette='viridis')
# plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)  # this line to show legend out side of box

# sns.countplot(x='Month', data=df, hue="Reason", palette='viridis')

GByMonth = df.groupby("Month").count()

# sns.countplot(x='Month', data=df, palette='viridis')

# sns.lmplot(x='Month', data=GByMonth.reset_index(), y='twp')

df['Date'] = df['timeStamp'].apply(lambda time: time.date())

# df[df['Reason']=='Traffic'].groupby('Date').count()['lat'].plot()
# df[df['Reason']=='Fire'].groupby('Date').count()['lat'].plot()
# df[df['Reason']=='EMS'].groupby('Date').count()['lat'].plot()

dayHour = df.groupby(by=["Day of Week", "Hour"]).count()["Reason"].unstack()

# sns.heatmap(dayHour, cmap='viridis')

sns.clustermap(dayHour, cmap='viridis')



plt.tight_layout()
plt.show()
