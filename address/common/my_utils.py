import re
import sys
import os
import logging
import datetime
import random
import calendar
from calendar import monthrange
# added new
from dateutil import relativedelta
logging.basicConfig(filename=f"debug/{datetime.date.today()}.log", level=logging.DEBUG, format='%(asctime)s:%(levelname)s:%(message)s')


def range_percentage_random(data, lo, hi):
    parsent = random.randrange(lo, hi)/100
    return int(data*parsent)+data


def range_random(lo, hi):
    return random.randrange(lo, hi)


def replace_string(data):
    data = data.replace(
        "\"", " "
    ).replace(
        "\'", " "
    )
    return data


def get_error_message_with_path(error):
    exc_tb = sys.exc_info()[2]
    error = f"{os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]}, {exc_tb.tb_lineno}, {error}"
    return replace_string(error)


def get_error_message(error):
    return replace_string(str(error))


def print_error(error):
    error = get_error_message(error)
    logging.warning(error)
    raise


def print_error_info(error):
    error = get_error_message_with_path(error)
    error = get_error_message(error)
    logging.warning(error)


def print_debug(message):
    logging.debug(str(message))


def print_info(message):
    logging.info(str(message))


def split_list(data_list, n):
    copy_data = []
    for i in range(0, len(data_list), n):
        copy_data.append(data_list[i:i + n])
    return copy_data


def str_to_num_list(string):
    string = str(string)
    return re.findall(r'\d+', string)


def date_to_date_list(date):
    return str_to_num_list(date)


def month_last_date(date):
    date = date_to_date_list(date)
    date[2] = str(monthrange(int(date[0]), int(date[1]))[1])
    return '-'.join(date)


def get_this_date():
    today = str(datetime.date.today())
    return today


def get_this_month_first_date():
    date = date_to_date_list(str(datetime.datetime.today()))
    return f"{date[0]}-{date[1]}-01"


def get_this_day():
    return date_to_date_list(str(datetime.datetime.today()))[2]


def get_this_hour():
    return date_to_date_list(str(datetime.datetime.today()))[3]


def get_month_initial(month):
    month_name = calendar.month_name[int(month)]
    return month_name[:3]


def as_datetime_look(string_list):
    if len(string_list) < 3:
        return None
    for _ in range(len(string_list), 7):
        string_list.append('0')
    return string_list


def str_date_or_time_to_datetime_list(string):
    string_list = str_to_num_list(string)
    string_list = as_datetime_look(string_list)
    date_list = [int(x) for x in string_list]
    return date_list


# added new
def convert_date_time(date_time):
    str_datetime = str(date_time)
    date_list = str_date_or_time_to_datetime_list(str_datetime)
    return datetime.datetime(*date_list)  # send datetime as a list of argument


def remove_datetime_error(data):
    data = list(data)
    for i in range(0, len(data)):
        if type(data[i]).__name__ in 'datetime':
            data[i] = convert_date_time(data[i])
    return data


def get_prev_month_first_date():
    today = str(datetime.datetime.today())
    date = [int(x) for x in re.findall(r'\d+', today)][:]
    date[1] -= 1
    if date[1] < 1:
        date[0] -= 1
        date[1] = 12
    if date[1] < 10:
        date[1] = str('0'+str(date[1]))
    return f"{date[0]}-{date[1]}-01"


# added new
def get_next_month(date):
    date_time = convert_date_time(date)
    nextmonth = date_time.date() + relativedelta.relativedelta(months=1)
    return nextmonth


# added new
def get_next_nth_month(date, n):
    date_time = convert_date_time(date)
    nextmonth = date_time.date() + relativedelta.relativedelta(months=n)
    return nextmonth


# added new
def diff_month(d1, d2):
    return (d2.year - d1.year) * 12 + d2.month - d1.month


def recursive_items(dictionary):
    for key, value in dictionary.items():
        if type(value) is dict:
            yield from recursive_items(value)
        else:
            yield (key, value)


def extract_fully(data, store):
    for key, value in recursive_items(data):
        if type(value) is list:
            store[key] = []
            for va in value:
                if type(va) is dict:
                    extract_fully(va, store)
                else:
                    store[key].append(va)
            if len(store[key]) <= 0:
                del store[key]
            return
        else:
            store[key] = value


def extract_nested_dict(data):
    result = {}
    extract_fully(data, result)
    return result


def make_dict(drrs_key, data):
    data_list = []
    for x in data:
        x = remove_datetime_error(x)
        data_list.append(dict(zip(drrs_key, x)))
    return data_list


if __name__ == "__main__":
    print(get_this_hour())
