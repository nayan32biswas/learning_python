import re
import sys
import os
import mysql.connector as mysql
from pymongo import MongoClient
from ..common.my_utils import print_error

try:
    sql_db = mysql.connect(
        host="localhost",
        user="root",
        passwd="nayan32biswas",
        database="a2i_new"
    )
except Exception as error:
    print_error(error)


class Mysql:
    def insert(query):
        try:
            cursor = sql_db.cursor()
            cursor.execute(query)
            sql_db.commit()
        except Exception as error:
            print("start error ")
            print_error(error)
            print("end")
        return True

    def insert_many(query_list):
        try:
            cursor = sql_db.cursor()
            for query in query_list:
                cursor.execute(query)
            sql_db.commit()
        except Exception as error:
            print_error(error)
        return True

    def get_column_name(query):
        try:
            cursor = sql_db.cursor()
            cursor.execute(query)
            column_name = cursor.fetchall()
        except Exception as error:
            print_error(error)
        table_key = [x[0] for x in column_name]
        return table_key

    def get_data(query):
        try:
            cursor = sql_db.cursor()
            cursor.execute(query)
            records = cursor.fetchall()
        except Exception as error:
            print_error(error)
        return records

    def update(query):
        try:
            cursor = sql_db.cursor()
            cursor.execute(query)
            sql_db.commit()
        except Exception as error:
            print_error(error)
        return True

    def update_many(querys):
        try:
            cursor = sql_db.cursor()
            for query in querys:
                cursor.execute(query)
            sql_db.commit()
        except Exception as error:
            print_error(error)
        return True


class Mongodb:
    def set_connection(self, db_name="a2i"):
        try:
            conn = MongoClient()
            mongo_db = conn[db_name]
        except Exception as error:
            print_error(error)
        return mongo_db, conn

    def insert_one(self, table_name, data):
        mongo_db, conn = self.set_connection()
        try:
            collection = mongo_db[table_name]
            collection.insert_one(data)
        except Exception as error:
            print_error(error)
        finally:
            conn.close()

    def insert_many(self, table_name, data_list):
        mongo_db, conn = self.set_connection()
        try:
            collection = mongo_db[table_name]
            for data in data_list:
                collection.insert_one(data)
        except Exception as error:
            print_error(error)
        finally:
            conn.close()

    def find(self, table_name, match_dict, projection=None):
        mongo_db, conn = self.set_connection()
        try:
            collection = mongo_db[table_name]
            if projection is not None:
                result = collection.find(match_dict, projection)
            else:
                result = collection.find(match_dict)
        except Exception as error:
            print_error(error)
        finally:
            conn.close()
        return result

    def find_one(self, table_name, match_dict, projection=None):
        result = []
        mongo_db, conn = self.set_connection()
        try:
            collection = mongo_db[table_name]
            if projection is not None:
                result = collection.find_one(match_dict, projection)
            else:
                result = collection.find_one(match_dict)
        except Exception as error:
            print_error(error)
        finally:
            conn.close()
        return result

    def update(self, table_name, match_dict, new_data):
        mongo_db, conn = self.set_connection()
        try:
            collection = mongo_db[table_name]
            result = collection.update_many(match_dict, new_data, True)
        except Exception as error:
            print_error(error)
        finally:
            conn.close()

    def insert_or_update(self, table_name, match_dict, new_data):
        mongo_db, conn = self.set_connection()
        try:
            collection = mongo_db[table_name]
            result = collection.update(match_dict, new_data, True)
        except Exception as error:
            print_error(error)
        finally:
            conn.close()

    def insert_or_update_many(self, table_name, new_data):
        mongo_db, conn = self.set_connection()
        try:
            collection = mongo_db[table_name]
            for x in new_data:
                collection.update_one(x[0], x[1], True)
        except Exception as error:
            print_error(error)
        finally:
            conn.close()
