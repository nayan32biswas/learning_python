import re
import datetime


time = datetime.datetime.now()
print(f"{time}\n")

print(time.year)
print(time.month)
print(time.day)
print(time.hour)
print(time.minute)
print(time.second)
print(time.microsecond)

print(f"\n{time}\n")
print("".join(re.findall("[^-:. ]+", str(time))))
