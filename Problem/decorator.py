"""
def decorator_one(func_one):
    def inside_func_one():
        print("\nExtra code before run func_one()")
        func_one()
        print("Extra code after run func_one()\n")
    return inside_func_one
def func_one():
    print("\tYou executing func_one()")
demo = decorator_one(func_one)
demo()

def default_decorator(default_func):
    def inside_default_func():
        print("\nExtra code before run default_func()")
        default_func()
        print("Extra code after run default_func()\n")

    return inside_default_func
@default_decorator
def default_func():
    print("\tYou executing default_func()")
default_func()
"""

""".................................................."""

"""
# decorator type function
def decorator_function(original_function):
    def wrapper_function():
        print("wrapper executed this before {}".format(original_function.__name__))
        return original_function()
    return wrapper_function
def display():
    print("display function")
decorated_display = decorator_function(display)
decorated_display()
"""
""".................................................."""
"""
# actual decorator
def decorator_function(original_function):
    def wrapper_function():
        print("wrapper executed this before {}".format(original_function.__name__))
        return original_function()
    return wrapper_function

@decorator_function
def display():
    print("display function")
display()
"""
""".................................................."""
"""
# advance decorator
def decorator_function(original_function):
    def wrapper_function(*args, **kwargs):
        print("wrapper executed this before {}".format(original_function.__name__))
        return original_function(*args, **kwargs)
    return wrapper_function

@decorator_function
def display():
    print("display function")

@decorator_function
def display_info(name, age):
    print("display_info with arguments ({}, {})".format(name, age))
display()
display_info("name", 23)
"""
""".................................................."""
"""
# class base decorator
class decorator_class(object):
    def __init__(self, original_function):
        self.original_function = original_function
    def __call__(self, *args, **kwargs):
        print("call executed this before {}".format(self.original_function.__name__))
        return self.original_function(*args, **kwargs)
@decorator_class
def display():
    print("display function")
    print()
@decorator_class
def display_info(name, age):
    print("display_info with arguments ({}, {})".format(name, age))
    print()
display()
display_info("name", 23)
"""
""".................................................."""




import time
from functools import wraps
def my_logger(orig_func):
    import logging
    logging.basicConfig(filename='{}.log'.format(orig_func.__name__), level=logging.INFO)

    @wraps(orig_func)
    def wrapper(*args, **kwargs):
        logging.info(
            'Ran with args: {}, and kwargs: {}'.format(args, kwargs))
        return orig_func(*args, **kwargs)

    return wrapper


def my_timer(orig_func):
    import time

    @wraps(orig_func)
    def wrapper(*args, **kwargs):
        t1 = time.time()
        result = orig_func(*args, **kwargs)
        t2 = time.time() - t1
        print('{} ran in: {} sec'.format(orig_func.__name__, t2))
        return result

    return wrapper


@my_logger
@my_timer
def display_info(name, age):
    time.sleep(1)
    print('display_info ran with arguments ({}, {})'.format(name, age))


display_info('Tom', 22)
