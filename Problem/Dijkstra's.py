import queue
import random
import sys


class GraphUndirectedWeighted(object):
    class Edge():
        def __init__(self, vertex, weight):
            self.vertex = vertex
            self.weight = weight

    def __init__(self, total_vertex):
        self.total_vertex = total_vertex
        self.adjacency_list = [[] for _ in range(total_vertex)]

    def add_edge(self, source, dest, weight):
        assert source < self.total_vertex
        assert dest < self.total_vertex
        self.adjacency_list[source].append(self.Edge(dest, weight))
        self.adjacency_list[dest].append(self.Edge(source, weight))

    def dijkstra(self, source, dest):
        que = queue.PriorityQueue()

        distances = [float("inf")] * self.total_vertex
        parents = [None] * self.total_vertex
        visited = [False] * self.total_vertex

        distances[source] = 0
        que.put((0, source))

        outter, inner = 0, 0

        while not que.empty():
            v_tuple = que.get()
            v = v_tuple[1]
            if visited[v] is not True:
                outter += 1
                for e in self.adjacency_list[v]:
                    candidate_distance = distances[v] + e.weight
                    inner += 1
                    if distances[e.vertex] > candidate_distance and visited[e.vertex] is not True:
                        distances[e.vertex] = candidate_distance
                        parents[e.vertex] = v
                        que.put((distances[e.vertex], e.vertex))
                visited[v] = True

        print(outter, inner)

        shortest_path = []
        end = dest
        while end is not None:
            shortest_path.append(end)
            end = parents[end]
        shortest_path.reverse()

        return shortest_path, distances[dest]


if __name__ == "__main__":
    N = 10
    g = GraphUndirectedWeighted(N)

    g.add_edge(0, 1, 5)
    g.add_edge(0, 2, 9)
    g.add_edge(0, 4, 6)
    g.add_edge(1, 2, 3)
    g.add_edge(1, 7, 9)
    g.add_edge(2, 3, 1)
    g.add_edge(3, 4, 2)
    g.add_edge(3, 6, 7)
    g.add_edge(3, 8, 5)
    g.add_edge(4, 5, 2)
    g.add_edge(5, 9, 7)
    g.add_edge(6, 9, 8)
    print(g.dijkstra(0, 6))
