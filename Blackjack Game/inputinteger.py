def startinput(s):
    while True:
        try:
            lll = input(s)
            n = [int(s) for s in lll.split() if s.isdigit()]
        except:
            continue
        else:
            break
    return n


def take(s="input valid integer: ", n=1):
    integer_list = list()
    while len(integer_list) < n:
        integer_list.extend(startinput(s))
    return integer_list


print(take("", 5))
