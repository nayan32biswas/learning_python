class TreeNode(object):
    def __init__(self, data):
        self.data, self.height = data, 1
        self.left, self.right = None, None


class AVL_Tree(object):
    def __init__(self):
        self.root = None

    def insert(self, data):
        self.root = self.__insert(self.root, data)

    def delete(self, data):
        self.root = self.__delete(self.root, data)

    def preOrder(self):
        self.__preOrder(self.root)

    def __insert(self, root, data):
        if not root:
            return TreeNode(data)
        elif data < root.data:
            root.left = self.__insert(root.left, data)
        else:
            root.right = self.__insert(root.right, data)
        root.height = 1 + max(self.__getHeight(root.left), self.__getHeight(root.right))
        balance = self.__getBalance(root)
        if balance > 1 and data < root.left.data:
            return self.__rightRotate(root)
        if balance < -1 and data > root.right.data:
            return self.__leftRotate(root)
        if balance > 1 and data > root.left.data:
            root.left = self.__leftRotate(root.left)
            return self.__rightRotate(root)
        if balance < -1 and data < root.right.data:
            root.right = self.__rightRotate(root.right)
            return self.__leftRotate(root)
        return root

    def __delete(self, root, data):
        if not root:
            return root
        elif data < root.data:
            root.left = self.__delete(root.left, data)
        elif data > root.data:
            root.right = self.__delete(root.right, data)
        else:
            if root.left is None:
                temp = root.right
                root = None
                return temp
            elif root.right is None:
                temp = root.left
                root = None
                return temp
            temp = self.__getMinValueNode(root.right)
            root.data = temp.data
            root.right = self.__delete(root.right, temp.data)
        if root is None:
            return root
        root.height = 1 + max(self.__getHeight(root.left), self.__getHeight(root.right))
        balance = self.__getBalance(root)
        if balance > 1 and self.__getBalance(root.left) >= 0:
            return self.__rightRotate(root)
        if balance < -1 and self.__getBalance(root.right) <= 0:
            return self.__leftRotate(root)
        if balance > 1 and self.__getBalance(root.left) < 0:
            root.left = self.__leftRotate(root.left)
            return self.__rightRotate(root)
        if balance < -1 and self.__getBalance(root.right) > 0:
            root.right = self.__rightRotate(root.right)
            return self.__leftRotate(root)
        return root

    def __leftRotate(self, z):
        y = z.right
        T2 = y.left
        y.left, z.right = z, T2  # Update heights
        z.height = 1 + max(self.__getHeight(z.left), self.__getHeight(z.right))
        y.height = 1 + max(self.__getHeight(y.left), self.__getHeight(y.right))
        return y

    def __rightRotate(self, z):
        y = z.left
        T3 = y.right
        y.right, z.left = z, T3  # Update heights
        z.height = 1 + max(self.__getHeight(z.left), self.__getHeight(z.right))
        y.height = 1 + max(self.__getHeight(y.left), self.__getHeight(y.right))
        return y

    def __getHeight(self, root):
        if not root:
            return 0
        return root.height

    def __getBalance(self, root):
        if not root:
            return 0
        return self.__getHeight(root.left) - self.__getHeight(root.right)

    def __getMinValueNode(self, root):
        if root is None or root.left is None:
            return root
        return self.__getMinValueNode(root.left)

    def __preOrder(self, root):
        if not root:
            return
        print(f"{root.data} ", end="")
        self.__preOrder(root.left)
        self.__preOrder(root.right)


if __name__ == '__main__':
    myTree = AVL_Tree()
    for _ in range(4):
        myTree.insert(_)
    myTree.preOrder()
    print()
    myTree.delete(1)
    myTree.preOrder()
    # 30 20 10 25 40 50
    print()


