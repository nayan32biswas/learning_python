from PIL import Image
import os
import time
import shutil
BASE_DIR = "/home/nayan32biswas/Public/programming/ai_ml_ds/file_sys_banchmark/"
PREV_DIR = "/home/nayan32biswas/Public/programming/ai_ml_ds/file_sys_banchmark/"
filename = "image.jpg"


def make_directory(path):
    if not os.path.exists(path):
        try:
            os.mkdir(path)
            return True
        except:
            if path == BASE_DIR:
                return
            prev = path.split("/")[:-1]
            prev = "/".join(prev)
            make_directory(prev)
            if not os.path.exists(path):
                os.mkdir(path)
            return True
    if not os.path.exists(path):
        os.mkdir(path)
    return False


def __copy_file(src, dst, rename):
    make_directory(dst)
    shutil.copy2(src, dst+rename)


def copy_file(src, dst, rename):
    src = PREV_DIR+src
    dst = BASE_DIR+dst
    __copy_file(src, dst, rename)


def flat_file(Size, next_dir):
    for i in range(Size):
        src = filename
        dst = f"{next_dir}/"
        rename = f"{i}{filename}"
        copy_file(src, dst, rename)


def deep_file(Size, next_dir):
    for i in range(Size):
        src = filename
        dst = f"{next_dir}/{i//1000000}/{i//10000}/{i//100}/"
        rename = f"{i}{filename}"
        copy_file(src, dst, rename)


SIZE = 1000000
# SIZE = 100
dist_flat_file = "flat_file"
dist_deep_file = "deep_file"

total = time.time()
flat_file(SIZE, dist_flat_file)
print(f"Flat File take {time.time()-total:.3f}s")

total = time.time()
deep_file(SIZE, dist_deep_file)
print(f"Deep File take {time.time()-total:.3f}s")
