import pandas as pd
import numpy as np
import cufflinks as cf
import matplotlib.pyplot as plt


from plotly.offline import download_plotlyjs,  plot, iplot


cf.go_offline()

df = pd.DataFrame(np.random.randn(100, 4), columns='A B C D'.split())

df1 = pd.DataFrame({'Category': ['A', 'B', 'C'], 'Values': [32, 43, 50]})

df.plot()
# df.iplot()  # this is very powerful plot tool but it is online. it work when code connected with plotly wesite


plt.show()
