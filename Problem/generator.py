def fibon_generator(n):
    a, b = 1, 1
    for i in range(n):
        yield a
        a, b = b, a + b


# this system don't take memory
for f in fibon_generator(10):
    print(f)

# same as
# this system take memory to store as a list
fib_list = list(fibon_generator(10))
print(fib_list)

temp = fibon_generator(10)
print(f"next value is: {next(temp)}")
print(f"then next value is: {next(temp)}")
print(f"then next value is: {next(temp)}")
print(f"then next value is: {next(temp)}")
print(f"then next value is: {next(temp)}")
print(f"then next value is: {next(temp)}")
# if you over n then you get error


print("\n")


def _power(n, p):
    for i in range(n):
        yield i ** p


# this system don't take memory
for f in _power(10, 7):
    print(f)

# same as
# this system take memory to store as a list
power_list = list(_power(10, 7))
print(power_list)

temp1 = _power(10, 6)
print(f"next value is: {next(temp1)}")
print(f"then next value is: {next(temp1)}")
print(f"then next value is: {next(temp1)}")
print(f"then next value is: {next(temp1)}")
print(f"then next value is: {next(temp1)}")
print(f"then next value is: {next(temp1)}")
# if you over n then you get error
