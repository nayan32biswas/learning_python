import numpy as np
arr = np.array([1, 2, 3, 4])
arr = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
arr = np.arange(0, 20, 3)
arr = np.zeros(10)
arr = np.zeros((2, 4))
arr = np.ones(10)
arr = np.ones((3, 2))
arr = np.linspace(0, 10, 90)

arr = np.eye(5)  # identity matrix

arr = np.random.rand(10)  # One dimensional random number
arr = np.random.rand(10, 10)  # Two dimensional random number

arr = np.random.randn(10)  # One dimensional random number
arr = np.random.randn(10, 10)  # Two dimensional random number

value = np.random.randint(1, 100+1)
arr = np.random.randint(1, 100+1, 10)

arr = np.array([1, 4, 2, 19, 20, 5])
max = arr.max()
max_index = arr.argmax()

min = arr.min()
min_index = arr.argmin()

reshape = arr.reshape(2, 3)  # 1D to 2D matrix
shape = reshape.shape

# numpy array support slicing

arr = np.array([
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
])
# print(arr[:2, 1:])  # this not suppoet in core python

arr = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9])
bool_arr = arr % 3 == 0
finded = arr[bool_arr]
# same as
finded = arr[arr % 3 == 0]


arr = np.random.randint(1, 100, 10)
add = arr+100
power = arr**3

arr = np.random.randint(1, 100, 10)
arr1 = np.random.randint(1, 100, 10)
add = arr+arr1
sub = arr-arr1
mult = arr*arr1
div = arr/arr1

sign = np.sin(arr)


arr = np.arange(10)
arr[:] = 5
print(arr)

print(np.arange(10, 51))
print(np.arange(10, 51, 2))

arr = np.arange(9)
arr = arr.reshape(3, 3)
print(arr)

print(np.eye(3))

print(np.random.rand(1))
print(np.random.randn(25))
print(np.linspace(0, 1, 100))
print(np.linspace(0, 1, 20))

mat = np.arange(1, 26).reshape(5, 5)

print(mat[2:, 1:])
