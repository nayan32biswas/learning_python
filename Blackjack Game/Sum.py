def single_sum(x):
    x = x.split(" ")[1]
    if x == "A":
        return 11
    elif x == "K" or x == "Q" or x == "J":
        return 10
    return int(x)


def _sum(card_list):
    if type(card_list) == str:
        return single_sum(card_list)
    return sum(list(map(single_sum, card_list)))
