from matplotlib import pyplot as plt
import numpy as np
import pandas as pd


def mean(X):
    sum = 0
    for x in X:
        sum += x
    return sum/len(X)


def slop(X, Y):
    XM = mean(X)
    m = ((XM*mean(Y)-mean(X*Y))/(XM*XM-mean(X*X)))
    return m


def intercept_slop(X, Y):
    m = slop(X, Y)
    b = (mean(Y)-m*mean(X))
    return m, b


file = 'data.xlsx'
data = pd.read_excel(file)
some_age = np.linspace(1, 100, 25)

X = data["age"].values
Y = data["emotion"].values
m, b = intercept_slop(X, Y)
a = [(m*x)+b for x in some_age]

plt.scatter(X, Y)
plt.plot(some_age, a, color='red', linewidth=3)
plt.xlabel("Age")
plt.ylabel("Emotion")

plt.show()
