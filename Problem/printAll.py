def stringPrint(s):
    if len(s) > 0:
        print(s.strip())  # lstrip remove leding space


def printFunction(data):
    s = ""
    if type(data) == dict:
        for key, value in data.items():
            if type(value) == int or type(value) == float or type(value) == str:
                s += " " + str(value)
            else:
                stringPrint(s)
                s = ""
                printFunction(value)
                stringPrint(s)

    elif type(data) == int or type(data) == float or type(data) == str:
        stringPrint(data)
    else:
        for value in data:
            if type(value) == int or type(value) == float or type(value) == str:
                s += " " + str(value)
            else:
                stringPrint(s)
                s = ""
                printFunction(value)
        stringPrint(s)


d = {
    "k1": "hello",
    "k4": {
        "k41": "world",
        "k42": [2, 1, {
            "k421": "hello world",
            "k422": [5, 6, 1.55],
            "k423": (4, 3, 9)
        }, 50, 3],
        "k43": (6, 1, 9),
    },
    "k2": [2, 1, 3, (3, 5, 2), {
        "k21": ["hello", 2, 1, 4]
    }],
    "k3": (2, 4, 6, 1, 9)
}
printFunction(d)
