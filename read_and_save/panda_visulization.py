import seaborn as sea
import numpy as num
import matplotlib.pyplot as plt
import pandas as pd

# df = sea.load_dataset("tips")
# df = pd.DataFrame(df)

# df = df.head(50)

# df['total_bill'].plot.hist()
# df.plot.bar(stacked=True)

# df.plot.scatter(x='tip', y='total_bill')
# df.plot.box()


df = pd.DataFrame(num.random.randint(1, 100, size=(300, 4)), columns=['a', 'b', 'c', 'd'])


# df.plot.hexbin(x='a', y='b', gridsize=20)
# df.plot.density()

# df.plot.area()

# df['a'].hist()

# df.plot.line(x='a',y='b')  # error

df.plot.scatter(x='a', y='b', s=df['c'])

plt.show()
