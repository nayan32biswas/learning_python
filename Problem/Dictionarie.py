d = {
    'list': [5, 2, 1, 4],
    'string': "hello world",
    'int': 3.14159,
    'dictionaries': {
        'list': [5, 2, 1, 4],
        'string': "hello world",
        'int': 3.14159,
        'int': 3.14
    }
}
print(d)
dic = "dictionaries"
myList = "list"
d[dic][myList].reverse()
print(d[dic][myList])

d[dic][myList].sort()
print(d[dic][myList])

print(d.keys())
print(d.values())
print(d.items())