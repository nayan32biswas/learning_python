import seaborn as sea
import numpy as np
import matplotlib.pyplot as plt

tips = sea.load_dataset('tips')
flights = sea.load_dataset('flights')

# tc = tips.corr()
# sea.heatmap(tc, cmap='coolwarm')

ft = flights.pivot_table(index='month', columns='year', values='passengers')


# sea.heatmap(ft, cmap='Greens', linecolor='white', linewidths=2)

sea.clustermap(ft, cmap='Greens', standard_scale=1)


plt.show()
