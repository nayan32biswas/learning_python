import requests
import time


def read_url(url):
    begin = time.time()
    try:
        req = requests.request(
            method=url.get("method"),
            url=url.get("url"),
            headers=url.get("headers"),
            files=url.get("files"),
            data=url.get("data"),
            params=url.get("params"),
            auth=url.get("auth"),
            cookies=url.get("cookies"),
            hooks=url.get("hooks"),
            json=url.get("json"),
            verify=url.get("verify", True),
        )
    except Exception as error:
        print("error is -> ", error)
        return False
    if req.status_code != 200:
        raise Exception(req.raise_for_status())
    print(f"url read completly in {time.time()-begin:.3f}s")

    web_data = req.content.decode("utf-8")
    logf = open("web_data.txt", "w")
    logf.write(web_data)
    return web_data


if __name__ == "__main__":
    url = {
        "method": "GET",
        "url": "https://www.airbnb.com/",
        "headers": None,
        "files": None,
        "data": None,
        "params": None,
        "auth": None,
        "cookies": None,
        "hooks": None,
        "json": None,
        "verify": None,
    }
    data = read_url(url)
