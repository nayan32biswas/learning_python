import time
import random
from celery import Celery
from script.operation.first_app import sum

app = Celery(
    'first_app',
    broker='amqp://localhost//',
    # backend='db_mysql://root:nayan32biswas@localhost/celery'
)
@app.task
def start():
    for _ in range(10):
        sum(random.randint(1, 1000), random.randint(100, 500))
