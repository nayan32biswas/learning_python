import urllib.request
import os

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

data = urllib.request.urlretrieve(
    "http://dashboard.a2i.gov.bd/public/assets/img/a2i_logo.png",
    BASE_DIR+"/a2i_logo.png"
)
