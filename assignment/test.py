import random
logf = open("output.txt", "w")


def is_positive():
    if random.randint(1, 100) < 50:
        return -1
    return 1

def range_percentage_random(data, lo, hi):
    parsent = random.randrange(lo, hi)/100
    return data*parsent

emoition = list()

for i in range(1000):
    age = random.randint(1, 100)
    if 0 <= age <= 5: x = 20
    elif 6 <= age <= 8: x = 40
    elif 9 <= age <= 10: x = 60
    elif 11 <= age <= 12: x = 70
    elif 13 <= age <= 15: x = 90
    elif 16 <= age <= 20: x = 85
    elif 21 <= age <= 25: x = 60
    elif 26 <= age <= 30: x = 55
    elif 31 <= age <= 35: x = 56
    elif 36 <= age <= 40: x = 50
    elif 41 <= age <= 45: x = 35
    elif 46 <= age <= 50: x = 33
    elif 51 <= age <= 55: x = 31
    elif 56 <= age <= 60: x = 25
    elif 61 <= age <= 65: x = 20
    elif 66 <= age <= 70: x = 18
    else: x = 15
    emoition.append((age, x+range_percentage_random(x, 1, 10)*is_positive()))

for age, e in emoition:
    logf.write(f"{age}\t{e}\n")