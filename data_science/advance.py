import pandas
from matplotlib import pyplot

data = pandas.read_csv("countries.csv")
low, hi = 10**6, 10**7
country = ["Bangladesh", "India", "Pakistan"]

country_data = [data[data.country == x] for x in country]

for index, value in enumerate(country):
    pyplot.plot(country_data[index].year, country_data[index].population/country_data[index].population.iloc[0]*100)

pyplot.xlabel("Year")
pyplot.ylabel("Population growth by year")
pyplot.legend(country)

pyplot.show()
