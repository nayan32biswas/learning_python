import pandas as pd
import numpy as np
import cufflinks as cf
import matplotlib.pyplot as plt
import plotly.graph_objs as go
from plotly.offline import download_plotlyjs,  plot, iplot


def demo_data_test():
    data = dict(
        type='choropleth',
        locations=["AZ", "CA", "NY"],
        locationmode='USA-states',
        colorscale='Greens',
        text=['text 1', 'text 2', 'text 3'],
        z=[1.0, 2.0, 3.0],
        colorbar={
            'title': 'Colorbar Title Goes Here'
        }
    )
    layout = dict(geo={'scope': 'usa'})
    choromap = go.Figure(data=[data], layout=layout)
    plot(choromap)


def usa_agreculter():
    df = pd.read_csv("2011_us_ag_exports.csv")
    data = dict(
        type='choropleth',
        locations=df['code'],
        locationmode='USA-states',
        colorscale='Hot',
        text=df['state'],
        z=df['total exports'],
        marker=dict(line=dict(
            color='rgb(255, 255, 255)',
            width=2,
        )),
        colorbar={
            'title': 'Million USD'
        }
    )
    layout = dict(
        title='2011 US Agriculter Exports by state',
        geo=dict(
            scope='usa',
            showlakes=True,
            lakecolor='rgb(85, 173, 240)'
        )
    )
    choromap = go.Figure(data=[data], layout=layout)
    plot(choromap)


def world_gdp():
    df = pd.read_csv("2014_world_gdp_with_codes.csv")
    data = dict(
        type='choropleth',
        locations=df['CODE'],
        text=df['COUNTRY'],
        z=df['GDP (BILLIONS)'],
        colorbar={
            'title': 'Billions USD'
        }
    )
    layout = dict(
        title='2014 Global GDP',
        geo=dict(
            showframe=False,
            projection={
                'type': 'mercator'
            }
        )
    )
    choromap = go.Figure(data=[data], layout=layout)
    plot(choromap)


world_gdp()

plt.show()
