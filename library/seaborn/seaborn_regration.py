import seaborn as sea
import numpy as np
import matplotlib.pyplot as plt


tips = sea.load_dataset("tips")

# sea.lmplot(
#     x='total_bill',
#     y='tip',
#     data=tips,
#     hue='sex',
#     markers=['+', 'o'],
#     scatter_kws={'s': 20}
# )

sea.lmplot(
    x='total_bill',
    y='tip',
    data=tips,
    col='day',
    hue='sex',
    aspect=0.6,
    size=8
)

plt.show()
