import pandas
from matplotlib import pyplot

file_data = pandas.read_csv("sample_data.csv")

pyplot.plot(file_data.column_a, file_data.column_b, )
pyplot.plot(file_data.column_a, file_data.column_c, "*")

pyplot.title("Plot")
pyplot.xlabel("x")
pyplot.ylabel("y or z")
pyplot.legend(["B", "C"])

pyplot.show()

