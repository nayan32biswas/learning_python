import seaborn as sea
import numpy as np
import matplotlib.pyplot as plt

tips = sea.load_dataset('tips')

# sea.distplot(tips['total_bill'], kde=False, bins=30)

# sea.jointplot(x='total_bill', y='tip', data=tips)
# sea.jointplot(x='total_bill', y='tip', data=tips, kind='hex')
# sea.jointplot(x='total_bill', y='tip', data=tips, kind='reg')
# sea.jointplot(x='total_bill', y='tip', data=tips, kind='kde')

# sea.pairplot(tips, hue='sex', palette='coolwarm')

# sea.rugplot(tips['total_bill'])

# sea.barplot(x='sex', y='total_bill', data=tips, estimator=np.std)

# sea.countplot(x='sex', data=tips)

# sea.boxenplot(x='day', y='total_bill', hue='smoker', data=tips)

# sea.violinplot(x='day', y='total_bill', hue='smoker', split=True, data=tips)

# sea.stripplot(x='day', y='total_bill', jitter=True, data=tips, hue='sex', dodge=True) # dodge is means split

# sea.swarmplot(x='day', y='total_bill', data=tips)

# sea.violinplot(x='day', y='total_bill', data=tips)
# sea.swarmplot(x='day', y='total_bill', data=tips, color='black')

# using factorplot we can drow any plot
# sea.factorplot(x='day', y='total_bill', data=tips, kind='swarm')  # assigning kind we can drow any type of plot


plt.show()
