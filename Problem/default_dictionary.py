from collections import defaultdict
from collections import OrderedDict

# act like c++ map if give key not exist its set as 0 that is give by lambda expression
d = defaultdict(lambda: 0)

print(d["one"])
d["two"] = 19
d["three"] = 9
d["four"] = 10
for k, v in d.items():
    print(k, v)
del d["three"]
print("\n")
for k, v in d.items():
    print(k, v)



print("\n\nStart OrderDict")
# more act like c++ map its flow insertion ordered
od = OrderedDict()

od["a"] = 12
od["b"] = 19
od["d"] = 9
od["c"] = 10
for k, v in od.items():
    print(k, v)
