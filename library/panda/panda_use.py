import numpy as np
import pandas as pd

arr = np.random.randint(1, 10, 4)
key_list = ['a', 'b', 'c', 'd']
d = {'a': 1, 'b': 2, 'c': 5, 'd': 6, }
series = pd.Series(arr)
series = pd.Series(arr, key_list)
series = pd.Series(d)

key_list = ['a', 'e', 'c', 'd']
serise1 = pd.Series(np.random.randint(1, 10, 4), key_list)
result = series+serise1

df = pd.DataFrame(np.random.randn(5, 3), ['A', 'B', 'C', 'D', 'E'], ['X', 'Y', 'Z'])
result = df
result = df['X']
result = df[['X', 'Y']]

df['New'] = df['X']+df['Y']
result = df

df.drop('Z', axis=1)  # but it's not remove any cloumn its return new DataFrame exept Z
result = df

# result = df.drop('Z', axis=1, inplace=True)
result = df

result = df.loc['A']  # two are sam
result = df.iloc[0]

result = df.loc['A', 'X']
result = df.loc[['A', 'B'], ['X', 'Y']]

booldf = df > 0
result = df[booldf]
result = df[df > 0]

booldf = df['X'] > 0
result = df[booldf]
result = df[df['X'] > 0]

resultdf = df[df['X'] > 0]
result = resultdf[['X', 'Y']]

resultdf = df[(df['X'] > 0) & (df['Y'] < 0)]  # conditional check
resultdf = df[(df['X'] > 0) | (df['Y'] < 0)]  # conditional check

new_index = [1, 2, 3, 4, 6]
result = df
result["full_new"] = new_index  # This also change df

resultdf_withindex = df.reset_index()
result = df.set_index('full_new')


arr = ['A', 'A', 'A', 'B', 'B', 'B']
index = [1, 2, 3, 1, 2, 3]
tuple_list = list(zip(arr, index))
lebels = pd.MultiIndex.from_tuples(tuple_list)
df = pd.DataFrame(np.random.randn(6, 2), lebels, ['X', 'Y'])

df.index.names = ['Grp', 'Num']

group_result = df.loc['A']
group_each_row_result = df.loc['A'].loc[1]
group_each_data_result = df.loc['A'].loc[1]['X']

result = df.xs(2, level='Num')


data = {
    'A': [np.nan, 3, 1],
    'B': [1, np.nan, 5],
    'C': [1, 2, 3]
}
df = pd.DataFrame(data)

rm_missing = df.dropna()
result = df.dropna(thresh=2)

# result = df.fillna(value="new")
result = df['A'].fillna(value=df['A'].mean())
result = df.fillna(value=df.mean())

data = {
    'Company': ['Google', 'Google', 'Facebook', 'Facebook'],
    'Person': ['Nayan', 'Hemel', 'Utshaw', 'Jeba'],
    'Sales': [3000, 2000, 1930, 1500]
}
df = pd.DataFrame(data)

gByCompany = df.groupby('Company')
result = gByCompany.mean()
result = gByCompany.sum()
result = gByCompany.std()  # Standard Deviation
result = gByCompany.count()
result = gByCompany.max()
result = gByCompany.min()
result = df.groupby('Company').mean().loc['Google']

result = gByCompany.describe()
result = gByCompany.describe().transpose()

data1 = {
    'Company': ['Google', 'Google', 'Facebook', 'Facebook'],
    'Person': ['Nayan', 'Hemel', 'Utshaw', 'Jeba'],
    'Sales': [2222, 1564, 1111, 1500]
}
data2 = {
    'Company': ['Google', 'Google', 'Facebook', 'Facebook'],
    'Person': ['Nayan', 'Noman', 'Jamil', 'Hamza'],
    'Sales': [1888, 2000, 1930, 1500]
}
data3 = {
    'Company': ['Twitter', 'Twitter', 'Microsoft', 'Microsoft'],
    'Person': ['Mobasher', 'Alif', 'Shuvo', 'Jeba'],
    'Sales': [1100, 2000, 1730, 1500]
}

df1 = pd.DataFrame(data1)
df2 = pd.DataFrame(data2)
df3 = pd.DataFrame(data3)

jone_three = pd.concat([df1, df2, df3])
jone_three = pd.concat([df1, df2, df3], axis=1)

left, right = df1, df2
marge_two = pd.merge(left, right, how='inner', on='Person')
marge_two = pd.merge(left, right, how='inner', on=['Company', 'Person'])

data = {
    'Company': ['Google', 'Google', 'Facebook', 'Facebook'],
    'Person': ['Nayan', 'Hemel', 'Utshaw', 'Jeba'],
    'Sales': [2222, 1564, 1111, 1500]
}
df = pd.DataFrame(data)

unique_value = df['Company'].unique()
unique_value = df['Company'].nunique()
unique_value = df['Company'].value_counts()


def own_func(x): return x**5


result = df['Sales'].apply(own_func)  # use custom function for each
result = df['Sales'].apply(lambda x: x//5)  # use lambda expresion for each
result = df['Person'].apply(len)  # use default function for each

result = df.columns
result = df.index

result = df.sort_values(by='Sales')

booldf = df.isnull()

data = {
    'A': ['f', 'f', 'f', 'b', 'b', 'b'],
    'B': ['o', 'o', 't', 't', 'o', 'o'],
    'C': ['x', 'y', 'x', 'y', 'x', 'y'],
    'D': [1, 2, 3, 4, 5, 6]
}
df = pd.DataFrame(data)

result = df.pivot_table(values='D', index=['A', 'B'], columns=['C'])


df = pd.read_excel("demo.xlsx")
# result = pd.read_excel("demo.xlsx",sheetname="Sheet")
# df.to_csv("demo1", index=False)
# result = df.to_excel("demo2.xlsx",index=False)
# result = df.to_excel("demo2.xlsx", sheet_name="Sheet1")

# df = pd.read_html("https://en.wikipedia.org/wiki/List_of_countries_by_population_(United_Nations)")
