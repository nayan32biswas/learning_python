import random

card = (
    "spades A", "spades K", "spades Q", "spades J", "spades 10", "spades 9", "spades 8", "spades 7", "spades 6",
    "spades 5", "spades 4", "spades 3", "spades 2",

    "hearts A", "hearts K", "hearts Q", "hearts J", "hearts 10", "hearts 9", "hearts 8", "hearts 7", "hearts 6",
    "hearts 5", "hearts 4", "hearts 3", "hearts 2",

    "diamonds A", "diamonds K", "diamonds Q", "diamonds J", "diamonds 10", "diamonds 9", "diamonds 8", "diamonds 7",
    "diamonds 6", "diamonds 5", "diamonds 4", "diamonds 3", "diamonds 2",

    "clubs A", "clubs K", "clubs Q", "clubs J", "clubs 10", "clubs 9", "clubs 8", "clubs 7", "clubs 6",
    "clubs 5", "clubs 4", "clubs 3", "clubs 2"
)


def getcardlist():
    cardlist = list(card)
    random.shuffle(cardlist)
    return cardlist
