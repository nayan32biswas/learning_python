import shutil
import os


class File():
    def __init__(self, *args, **kwargs):
        self.BASE_DIR = os.path.dirname(os.path.abspath(__file__))

    def path_top(self):
        return os.path.dirname(os.path.abspath(__file__))

    def path_top_1(self):
        return os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

    def path_top_2(self):
        return os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

    def path_top_3(self):
        return os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

    def path_top_4(self):
        return os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

    def path_top_5(self):
        return os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

    def path_top_n(self, n):
        case = {
            "0": self.path_top(),
            "1": self.path_top_1(),
            "2": self.path_top_2(),
            "3": self.path_top_3(),
            "4": self.path_top_4(),
            "5": self.path_top_5()
        }
        return case.get(str(n))

    def make_directory(self, dirName):
        path = self.BASE_DIR+dirName
        if not os.path.exists(path):
            os.mkdir(path)
            return True
        return False

    def copy_file(self, src, dst):
        src = self.BASE_DIR+src
        dst = self.BASE_DIR+dst
        shutil.copy2(src, dst)

    def copy_directory(self, src, dst):
        src = self.BASE_DIR+src
        dst = self.BASE_DIR+dst
        try:
            shutil.copytree(src, dst)
        except Exception as error:
            print(error)
            return "file alrady exists"


Ffile = File()

# Ffile.make_directory("/new_file")


# file_path = "/old_file"
# dst_folder = "/new_file/demo"
# Ffile.copy_directory(file_path, dst_folder)



