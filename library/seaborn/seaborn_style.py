import seaborn as sea
import numpy as np
import matplotlib.pyplot as plt


tips = sea.load_dataset("tips")

sea.set_style('ticks')
sea.set_context('poster', font_scale=2)
sea.countplot(x='sex', data=tips)
# sea.despine(left=True, right=True)

plt.show()
