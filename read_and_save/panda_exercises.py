import numpy as np
import pandas as pd

salaries = pd.read_csv('Salaries.csv')

result = salaries.head(2)

# salaries.info()

result = salaries['BasePay'].mean()

result = salaries['OvertimePay'].max()

result = salaries[salaries['EmployeeName'] == 'JOSEPH DRISCOLL']['JobTitle']

result = salaries[salaries['EmployeeName'] == 'JOSEPH DRISCOLL']['TotalPayBenefits']

result = salaries.loc[salaries['TotalPayBenefits'].idxmax()]
# result = salaries[salaries['TotalPayBenefits'].max() == salaries['TotalPayBenefits']]  # same as
# result = salaries.iloc[salaries['TotalPayBenefits'].argmax()]  # same as


result = salaries.loc[salaries['TotalPay'].idxmin()]
# result = salaries[salaries['TotalPayBenefits'].min() == salaries['TotalPayBenefits']]  # same as
# result = salaries.iloc[salaries['TotalPayBenefits'].argmin()]  # same as

result = salaries.groupby('Year').mean()['BasePay']

result = len(salaries['JobTitle'].unique())
# result = salaries['JobTitle'].nunique()  # same as

result = salaries.groupby('JobTitle').count().sort_values(by='Id', ascending=False).head(5)['Id']
# result = salaries['JobTitle'].value_counts().head(5)  # same as


value = salaries[salaries['Year'] == 2013].groupby('JobTitle').count()
result = len(value[value['Id'] == 1])
# result = sum(salaries[salaries['Year'] == 2013]['JobTitle'].value_counts() == 1)  # same as

result = len(salaries[salaries['JobTitle'].apply(lambda x: 'chief' in x.lower().split())])

salaries['title_len'] = salaries['JobTitle'].apply(len)
result = salaries[['TotalPayBenefits', 'title_len']].corr()


print(result)
