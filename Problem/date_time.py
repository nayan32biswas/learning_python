import re
import datetime

time = datetime.time(2, 8, 2, 50)
print(time)
print(f"hour is {time.hour}")
print(f"hour is {time.minute}")
print(f"hour is {time.second}")
print(f"hour is {time.microsecond}\n\n")


time = datetime.datetime.now()
print(f"{time}\n")

print(time.year)
print(time.month)
print(time.day)
print(time.hour)
print(time.minute)
print(time.second)
print(time.microsecond)

print("")
ID = int("".join(re.findall("[^-:. ]+", str(time))))
print(ID)
