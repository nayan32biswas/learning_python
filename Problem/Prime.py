def isPrime(n):
    root = int(n**.5 + 1)
    if n<2:
        return False
    if n == 2:
        return True
    for i in range(2, root):
        if n % i == 0:
            return False
    return True

def allPrime(n):
    primeList = []
    for i in range(n+1):
        if isPrime(i):
            primeList.append(i)
    return primeList
print(allPrime(100))