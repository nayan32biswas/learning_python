from matplotlib import pyplot as plt
import numpy as np

x = np.linspace(1, 50, 10)
y = x**1.3

# plt.plot(x, y)
# plt.xlabel("X label")
# plt.ylabel("Y label")
# plt.title("Title")

# plt.subplot(1, 2, 1)
# plt.plot(x, y, 'g')
# plt.subplot(1, 2, 2)
# plt.plot(y, x, 'r')

# fig = plt.figure()
# axes = fig.add_axes([0.1, 0.1, 0.8, 0.8])
# axes.plot(x, y)

# fig = plt.figure()
# axes = fig.add_axes([0.1, 0.1, 0.8, 0.8])
# axes1 = fig.add_axes([0.2, 0.5, 0.2, 0.2])

# axes.set_xlabel("X label")
# axes.set_ylabel("Y label")
# axes.set_title("Outter Plot")

# axes1.set_title("Inner Plot")
# axes1.plot(x, y)

# fig, axes = plt.subplots(nrows=1, ncols=5)
# # axes[0].plot(x, y)
# # axes[1].plot(y, x)
# for each in axes:  # same as
#     each.plot(x, y)
#     x, y = y[:], x[:]

# fig, axes = plt.subplots(nrows=2, ncols=1, figsize=(8, 8), dpi=320)
# axes[0].plot(x, y)
# axes[1].plot(y, x)
# fig.savefig("new_fig.png")  # This save fig in file

# fig = plt.figure()
# axes = fig.add_axes([0.1, 0.1, 0.8, 0.8])
# axes.plot(x, x**2, label='x square')
# axes.plot(x, x**3, label='x x quebe')
# axes.plot(x, x**4, label='x to the power 4')
# # here loc=0 means best location to draw legend
# axes.legend(loc=0)  # this line show legend for each line


fig = plt.figure()
axes = fig.add_axes([0.1, 0.1, 0.8, 0.8])
axes.plot(
    x,
    x**2,
    color="#6b03fc",
    linewidth=2,
    alpha=1,
    linestyle='dashdot',
    marker='o',
    markersize=10,
    markerfacecolor='yellow',
    markeredgewidth = 2,
    markeredgecolor='green'
)


plt.tight_layout()  # this line remove overlaping when multiple plot drow
plt.show()
