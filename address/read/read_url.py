import re
import time
import requests
import timeout_decorator
from ..common.my_utils import print_error, print_info


def replace_string(data):
    data = data.replace(
        ":null", ":'null'"
    ).replace(
        ": null", ": 'null'"
    ).replace(
        "null,", "'null',"
    )
    return data


@timeout_decorator.timeout(500)  # maximum execution time is 100 seconds
def read_api_from_url(url, **kwargs):
    begin = time.time()
    print(url)
    try:
        req = requests.request(
            method=url.get("method"),
            url=url.get("url"),
            headers=url.get("headers"),
            files=url.get("files"),
            data=url.get("data"),
            params=url.get("params"),
            auth=url.get("auth"),
            cookies=url.get("cookies"),
            hooks=url.get("hooks"),
            json=url.get("json"),
            verify=url.get("verify", True),
        )
    except Exception as error:
        print_error(error)
    if req.status_code != 200:
        raise Exception(req.raise_for_status())
    print_info(f"url read completly in {time.time()-begin:.3f}s")
    data = req.content.decode("utf-8")

    data = replace_string(data)

    if kwargs.get("dict"):
        data = eval(data)
    return data
