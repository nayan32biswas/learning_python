import re


def multi_re_find(patterns, phrase):
    for pattern in patterns:
        print(f"for pattern {pattern}")
        print(re.findall(pattern, phrase))
        print()


test_phrase = "sdsd.....sssddd....sdddsddd...dsds....dsssss....sdddd"
test_patterns = ["sd*", "sd+", "sd?", "sd{3}", "sd{2,3}"]
multi_re_find(test_patterns, test_phrase)
