import pandas
from matplotlib import pyplot

data = pandas.read_csv("countries.csv")
bangladesh = data[data.country == "Bangladesh"]
india = data[data.country == "India"]

pyplot.plot(bangladesh.year, bangladesh.population/bangladesh.population.iloc[0]*100)  # in milion
pyplot.plot(india.year, india.population/india.population.iloc[0]*100)  # in milion

pyplot.xlabel("Year")
pyplot.ylabel("Population growth per year")
pyplot.legend(["Bangladesh", "India"])

pyplot.show()
