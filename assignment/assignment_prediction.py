from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from sklearn import preprocessing
from sklearn import svm
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import LinearRegression
from sklearn.metrics import confusion_matrix

logf = open("output.txt", "w")

file = 'data.xlsx'
data = pd.read_excel(file)
prediction_data = np.linspace(1, 100, 20)

X = data["age"].values.reshape(-1, 1)
Y = data["emotion"].values.reshape(-1, 1)
make_prediction = prediction_data.reshape(-1, 1)

# X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.2, random_state=1000)

model = svm.SVC(gamma=0.001, C=200.)
lab_enc = preprocessing.LabelEncoder()

training_scores_encoded = lab_enc.fit_transform(Y)
model.fit(X, training_scores_encoded)
# logf.write(f"{model.intercept_}\n\n{model.classes_}\n\n")
p_predict = model.predict(make_prediction)

# training_scores_encoded = lab_enc.fit_transform(y_train)
# model.fit(X_train, training_scores_encoded)
# p_predict = model.predict(make_prediction)

logf.write(f"{p_predict}\n\n")

plt.scatter(X, Y)
plt.plot(make_prediction, p_predict, color='red')
plt.xlabel("Age")
plt.ylabel("Emotion")


plt.tight_layout()  # this line remove overlaping when multiple plot drow
plt.show()
