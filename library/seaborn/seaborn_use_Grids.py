import seaborn as sea
import numpy as np
import matplotlib.pyplot as plt

iris = sea.load_dataset('iris')

# grid = sea.PairGrid(iris)
# grid.map_diag(sea.distplot)
# grid.map_upper(plt.scatter)
# grid.map_lower(sea.kdeplot)

tips = sea.load_dataset("tips")

grid = sea.FacetGrid(tips, col='time', row='smoker')

# grid.map(sea.distplot, 'total_bill')
grid.map(plt.scatter, 'total_bill', 'tip')


plt.show()
